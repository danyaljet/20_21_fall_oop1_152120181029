#include<iostream>
#include<stdlib.h>
#include<fstream>
#include<string>

using namespace std;

int A[100];

/**
* the function opens the file and return the size value
*/
int openFile() {
    string fileName = "input.txt";
    fstream dataFile;
    int size = 1;

    dataFile.open(fileName, ios::in | ios::out);
    if (!dataFile) {
        cout << "Error! The file (" << fileName << ") cannot be opened!";
        exit(0);
    }
    for (int i = 0; i < size + 1; i++) {
        dataFile >> A[i];
        size = A[0];
    }

    return size;
}

/**
* this function finds the sum of the numbers
*/
int findSum(int size) {
    int sum = 0;
    for (int i = 1; i < size + 1; i++) {
        sum += A[i];
    }
    return sum;
}

/**
* this function finds the product of the numbers
*/
int findProduct(int size) {
    int product = 1;
    for (int i = 1; i < size + 1; i++) {
        product *= A[i];
    }
    return product;
}

/**
* this function finds the average of the numbers
*/
float findAvg(int sum, int size) {
    return (float)sum / size;
}

/**
* this function finds the smallest number from the array
*/
int findTheSmallest(int size) {
    int smallest = A[0];
    for (int i = 1; i < size + 1; i++) {
        if (A[i] < smallest)
            smallest = A[i];
    }
    return smallest;
}

int main() {
    /**
    * openFile function works and opens the file and returns size value
    */
    int size = openFile();

    cout << "Sum is " << findSum(size) << endl;
    cout << "Product is " << findProduct(size) << endl;
    cout << "Average is " << findAvg(findSum(size), size) << endl;
    cout << "Smallest is " << findTheSmallest(size) << endl;

    return 0;
}